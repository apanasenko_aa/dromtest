<?php defined('INDEX') OR die('Прямой доступ к странице запрещён!');

require_once("database_config.php");

class AuthController
{
    private $login;
    private $password;
    private $action;
    private $db;
    private $session = NULL;
    private $session_end = NULL;
    private $errors = [];


    function __construct($login, $password, $action)
    {
        $this->login = trim($login);
        $this->password = trim($password);
        $this->action = $action;
        $this->db = new mysqli(Database::$host, Database::$login, Database::$password, Database::$name);

        if ($this->db->connect_errno)
            $this->add_db_error("Не удалось установить соединение с базой данных");
    }


    function __destruct()
    {
        $this->db->close();
    }


    static function random_string($length = 6)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
        $code = "";
        $n = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++)
            $code .= $chars[mt_rand(0, $n)];

        return $code;
    }


    private function add_db_error($message){
        $this->errors[] = $message;
        $this->errors[] = "Не удалось подключиться к MySQL: (" . $this->db->connect_errno . ") " . $this->db->connect_error;
    }


    public function authorization()
    {
        if(!preg_match("/^[a-zA-Z0-9]+$/", $this->login))
            $this->errors[] = "Логин может состоять только из букв английского алфавита и цифр";

        if(strlen($this->login) < 3 or strlen($this->login) > 30)
            $this->errors[] = "Логин должен быть не меньше 3-х символов и не больше 30";

        if(strlen($this->password) < 3 or strlen($this->password) > 30)
            $this->errors[] = "Пароль должен быть не меньше 3-х символов и не больше 30";

        if ($this->action === 'signup')
            $this->sign_up();

        $this->sign_in();
    }


    public function sign_up()
    {
        if (count($this->errors)) return;

        if ($request = $this->db->prepare("SELECT COUNT(*) FROM users WHERE login=?"))
        {
            $request->bind_param("s", $this->login);
            $request->execute();
            $request->bind_result($count);
            $request->fetch();
            $request->close();
        }
        else
        {
            $this->add_db_error("Не удалось проверить логин на уникальность");
            return;
        }

        if ($count)
        {
            $this->errors[] = "Пользователь с таким именем уже зарегистрирован в системе";
            return;
        }

        if ($request = $this->db->prepare("INSERT INTO users (login, password) VALUES (?, ?)"))
        {
            $request->bind_param("ss", $this->login, md5(md5($this->password)));
            $request->execute();
            $request->close();
        }
        else
            $this->add_db_error("Не удалось добавить пользователя");
    }


    public function sign_in()
    {
        if (count($this->errors)) return;

        if ($request = $this->db->prepare("SELECT id, login, password FROM users WHERE login=?"))
        {
            $request->bind_param("s", $this->login);
            $request->execute();
            $request->bind_result($id, $login, $password);
            $request->fetch();
            $request->close();
        }
        else
        {
            $this->add_db_error("Не удалось проверить логин на уникальность");
            return;
        }

        if (!$login)
        {
            $this->errors[] = "Пользователь с таким именем не зарегистрирован в системе";
            return;
        }

        if ($password !== md5(md5($this->password)))
        {
            $this->errors[] = "Неверный пароль";
            return;
        }

        if ($request = $this->db->prepare("INSERT INTO sessions (user_id, sessions, end_time) VALUES (?, ?, ?)"))
        {
            $this->session = md5($this::random_string());
            $this->session_end = new DateTime();
            $this->session_end->modify("+7 day");
            $request->bind_param("iss",
                $id,
                $this->session,
                $this->session_end->format("Y-m-d H:i:s")
            );
            $request->execute();
            $request->close();
        }
        else
            $this->add_db_error("Не удалось добавить сессию");
    }


    public function get_session()
    {
        if (count($this->errors))
            return FALSE;

         return ['session' => $this->session, 'session_end' => $this->session_end];
    }


    public function get_errors()
    {
        if (!count($this->errors))
            return FALSE;

        return $this->errors;
    }
}
?>
