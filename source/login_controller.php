<?php defined('INDEX') OR die('Прямой доступ к странице запрещён!');

require_once("database_config.php");

class LoginController
{
    private $user_id = 0;
    private $session;
    private $errors = [];


    function __construct($session)
    {
        $this->session = $session;
        $this->db = new mysqli(Database::$host, Database::$login, Database::$password, Database::$name);

        if ($this->db->connect_errno)
            $this->add_db_error("Не удалось установить соединение с базой данных");
    }


    function __destruct()
    {
        $this->db->close();
    }


    private function add_db_error($message){
        $this->errors[] = $message;
        $this->errors[] = "Не удалось подключиться к MySQL: (" . $this->db->connect_errno . ") " . $this->db->connect_error;
    }


    public function is_login()
    {
        if (count($this->errors)) return FALSE;

        if (!$this->session)
        {
            $this->errors[] = "Неизвестная сесия";
            return FALSE;
        }

        if ($request = $this->db->prepare("SELECT user_id FROM sessions WHERE sessions=? AND end_time > NOW()"))
        {
            $request->bind_param("s", $this->session);
            $request->execute();
            $request->bind_result($user_id);
            $request->fetch();
            $request->close();
        }
        else
        {
            $this->add_db_error("Не удалось получить сессию");
            return FALSE;
        }

        if (!$user_id)
        {
            $this->errors[] = "Сессия устарела";
            return FALSE;
        }

        $this->user_id = $user_id;
        return TRUE;
    }


    public function get_user_id()
    {
        return $this->user_id;
    }
}
?>
