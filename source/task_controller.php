<?php defined('INDEX') OR die('Прямой доступ к странице запрещён!');

require_once("database_config.php");

class TaskController
{
    private $user_id = 0;
    private $errors = [];
    private $tasks = [];

    function __construct($user_id)
    {
        $this->user_id = $user_id;
        $this->db = new mysqli(Database::$host, Database::$login, Database::$password, Database::$name);

        if ($this->db->connect_errno)
            $this->add_db_error("Не удалось установить соединение с базой данных");
    }


    function __destruct()
    {
        $this->db->close();
    }


    private function add_db_error($message){
        $this->errors[] = $message;
        $this->errors[] = "Не удалось подключиться к MySQL: (" . $this->db->connect_errno . ") " . $this->db->connect_error;
    }


    public function all_tasks()
    {
        if (count($this->errors)) return [];

        if ($request = $this->db->prepare("SELECT id, text, is_completed FROM tasks WHERE user_id=?"))
        {
            $request->bind_param("i", $this->user_id);
            $request->execute();
            $request->bind_result($id, $text, $is_completed);

            while ($request->fetch())
                $this->tasks[] = ['id' => $id, 'text' => $text, 'is_completed' => $is_completed];

            $request->close();
        }
        else
        {
            $this->add_db_error("Не удалось получить список задач");
        }

        return $this->tasks;
    }


    public function add_task($task)
    {
        if (count($this->errors)) return [];

        if ($request = $this->db->prepare("INSERT INTO tasks (user_id, text) VALUES (?, ?)"))
        {
            $request->bind_param("is", $this->user_id, $task);
            $request->execute();
            $insert_id = $request->insert_id;
            $request->close();
        }
        else
        {
            $this->add_db_error("Не удалось получить добавить задачу");
            return [];
        }
        return ['id' => $insert_id, 'text' => $task];
    }


    public function remove_task($id)
    {
        $this->update_task(
            "DELETE FROM tasks WHERE id = ?",
            $id,
            "Не удалось получить удалить задачу"
        );
        return [];
    }


    public function completed_task($id)
    {
        $this->update_task(
            "UPDATE tasks SET is_completed = 1 WHERE id = ?",
            $id,
            "Не удалось получить завершить задачу"
        );
        return ['is_completed' => TRUE];
    }


    public function uncompleted_task($id)
    {
        $this->update_task(
            "UPDATE tasks SET is_completed = 0 WHERE id = ?",
            $id,
            "Не удалось получить возобновить задачу"
        );
        return ['is_completed' => FALSE];
    }


    private function update_task($sql, $id, $error_message)
    {
        if (count($this->errors)) return;

        if ($request = $this->db->prepare($sql))
        {
            $request->bind_param("i", $id);
            $request->execute();
            $request->close();
        }
        else
        {
            $this->add_db_error($error_message);
            $this->errors[] = "Не удалось подключиться к MySQL: (" . $this->db->connect_errno . ") " . $this->db->connect_error;
        }
    }


    public function get_errors()
    {
        if (!count($this->errors))
            return FALSE;

        return $this->errors;
    }
}
?>
