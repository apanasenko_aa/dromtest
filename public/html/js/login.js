function login(action)
{
    console.log($.cookie('user'));
    $('#errors').html('');
    $.post(
        '/index.php',
        {
            page: 'login',
            action: action,
            login: $('#login').val(),
            password: $('#password').val()
        },
        function(data, status){
            d = $.parseJSON(data);
            if (d.status == 'bad') {
                $('#errors').html(d.errors);
            } else {
                $.cookie('user', d.session, { expires: new Date(d.session_end)});
                location.reload();
            }

        }
    );
}
