$(document).ready(function(){
    $('#temp').hide();
    $('#add_todo').keypress(function (e) {
        if (e.which == 13) {
            add_task(this);
        }
    });
    $('.filters a').click(function(){
        filter(this);
    });

    $('.clear-completed').click(function(){
        $('.completed').each(function(){
            remove_task(this);
        });
    });
    $('#count_active').html($('.uncompleted').length);
});


function generate_task(id, text, is_completed){
    var new_task = $('#temp').clone();
    new_task.insertAfter($('#temp'));

    if (is_completed) {
        new_task.find('.toggle').prop('checked', true);
        new_task.attr('class', 'completed');
    } else {
        new_task.find('.toggle').prop('checked', false);
        new_task.attr('class', 'uncompleted');
    }

    new_task.find('#task_id').val(id);
    new_task.find('#text').html(text);
    new_task.attr('id', 'task');
    new_task.show();

    new_task.find('.destroy').click(function(){
        remove_task($(this).parents('#task'));
    });

    new_task.find('.toggle').change(function(){
        update_task_status($(this).parents('#task'), this);
    });
}


function add_task(block){
    $('#errors').html('');
    $.post(
        '/index.php',
        {
            user: $.cookie('user'),
            page: 'main',
            action: 'add',
            new_task: $(block).val()
        },
        function(data, status){
            d = $.parseJSON(data);
            if (d.status == 'bad') {
                $('#errors').html(d.errors);
            } else {
                $(block).val('');
                generate_task(d.id, d.text, false);
            }
            $('#count_active').html($('.uncompleted').length);
        }
    );
}


function remove_task(block){
    $('#errors').html('');
    $.post(
        '/index.php',
        {
            user: $.cookie('user'),
            page: 'main',
            action: 'remove',
            task_id: $(block).find('#task_id').val()
        },
        function(data, status){
            d = $.parseJSON(data);
            if (d.status == 'bad') {
                $('#errors').html(d.errors);
            } else {
                $(block).remove();
            }
            $('#count_active').html($('.uncompleted').length);
        }
    );
}


function update_task_status(block, checkbox){
    $('#errors').html('');
    $.post(
        '/index.php',
        {
            user: $.cookie('user'),
            page: 'main',
            action: $(checkbox).is(':checked') ? 'completed' : 'uncompleted',
            task_id: $(block).find('#task_id').val()
        },
        function(data, status){
            d = $.parseJSON(data);
            if (d.status == 'bad') {
                $('#errors').html(d.errors);
            } else {
                $(block).attr('class', d.is_completed ? 'completed' : 'uncompleted');
                $(checkbox).prop('checked', d.is_completed);
            }
            $('#count_active').html($('.uncompleted').length);
        }
    );
}


function filter(block){
    var id = $(block).attr('id');
    $('.filters a').removeAttr('class');
    $(block).attr('class', 'selected');
    if (id == 'active'){
        $('.uncompleted').show();
        $('.completed').hide();
    }
    else if (id == 'completed'){
        $('.completed').show();
        $('.uncompleted').hide();
    }
    else if (id == 'all'){
        $('.completed, .uncompleted').show();
    }
}
