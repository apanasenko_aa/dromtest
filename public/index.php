<?php session_start();

    define("INDEX", "");

    function respons_404()
    {
        header("HTTP/1.0 404 Not Found");
        exit(0);
    }

    #require_once($_SERVER[DOCUMENT_ROOT] . "/../source/auth_controller.php);
    require_once("../source/login_controller.php");
    require_once("../source/auth_controller.php");
    require_once("../source/task_controller.php");

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        if (!isset($_POST['page']))
            respons_404();

        if ($_POST['page'] === 'login')
        {
            $user = new AuthController($_POST['login'], $_POST['password'], $_POST['action']);
            $user->authorization();
            $errors = $user->get_errors();
            if ($errors)
                print(json_encode([
                    'status' => 'bad',
                    'errors' => join('<br>', $errors)
                ]));
            else
                print(json_encode(['status' => 'ok'] + $user->get_session()));
        }
        else if ($_POST['page'] === 'main')
        {
            $user = new LoginController($_POST['user']);
            if (!$user->is_login())
                respons_404();

            $tasks = new TaskController($user->get_user_id());
            switch ($_POST['action'])
            {
                case 'add':
                    $results = $tasks->add_task($_POST['new_task']);
                    break;

                case 'remove':
                    $results = $tasks->remove_task($_POST['task_id']);
                    break;

                case 'completed':
                    $results = $tasks->completed_task($_POST['task_id']);
                    break;

                case 'uncompleted':
                    $results = $tasks->uncompleted_task($_POST['task_id']);
                    break;

                default:
                    respons_404();
                    break;
            }

            $errors = $tasks->get_errors();
            if ($errors)
                print(json_encode([
                    'status' => 'bad',
                    'errors' => join('<br>', $errors)
                ]));
            else
                print(json_encode(['status' => 'ok'] + $results));
        }
    }
    else if ($_SERVER['REQUEST_METHOD'] === 'GET')
    {
        $user = new LoginController($_COOKIE["user"]);
        if ($user->is_login())
        {
            $tasks = new TaskController($user->get_user_id());
            $tasks_list = $tasks->all_tasks();
            include('html/main.html');
        }
        else
            include('html/login.html');
    }
    else
        respons_404();
?>
