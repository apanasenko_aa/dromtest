DROP TABLE IF EXISTS `sessions`;
DROP TABLE IF EXISTS `tasks`;
DROP TABLE IF EXISTS `users`;

--
-- Структура таблицы `Users`
--
CREATE TABLE IF NOT EXISTS `users` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `login` varchar(30) NOT NULL,
    `password` varchar(32) NOT NULL,
    UNIQUE(`login`),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


--
-- Структура таблицы `Sessions`
--
CREATE TABLE IF NOT EXISTS `sessions` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) unsigned NOT NULL,
    `sessions` varchar(32) NOT NULL,
    `end_time` datetime NOT NULL,
    FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


--
-- Структура таблицы `Tasks`
--
CREATE TABLE IF NOT EXISTS `tasks` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) unsigned NOT NULL,
    `is_completed` boolean NOT NULL default 0,
    `text` text NOT NULL,
    FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
